#include<iostream>
#include<cstdio>
#include <vector>
using namespace std;


int main() {
    long n;
    while (scanf("%ld", &n), n != 0) {
        int ck = 0;
        std::vector<char> out;
        for (int i = 0; n != 0; i++) {
            if (n & 1) {
                out.push_back('1');
                ck++;
            } else out.push_back('0');
            n = n >> 1;
        }
        printf("The parity of ");
        for (int i = out.size() - 1; i >= 0; i--)
            printf("%c", out[i]);
        printf(" is %d (mod 2).\n", ck);
    }

    return 0;
}
